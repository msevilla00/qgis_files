---
title: "QGIS Python Plugins"
author: "Miguel Sevilla-Callejo"
date: "20180815"
---

# QGIS Phyton Plugins

**CC-BY-SA** 2018
*Miguel Sevilla-Callejo*

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->

* [Links](#links)
* [Must have Plug-ins](#must-have-plug-ins)
	* [mmqgis](#mmqgis)
	* [QuickOSM](#quickosm)
	* [Qgis2threejs](#qgis2threejs)
	* [qgis2web](#qgis2web)
	* [OpenLayers Plugin](#openlayers-plugin)
	* [QConsolidate](#qconsolidate)
	* [QNEAT3 - QGIS Network Analysis Toolbox 3](#qneat3-qgis-network-analysis-toolbox-3)
	* [Layer Board](#layer-board)
	* [QuickMapServices](#quickmapservices)
* [Interesting Plugins](#interesting-plugins)
	* [QSpatiaLite](#qspatialite)
	* [Profile tool](#profile-tool)
	* [Table Manager](#table-manager)
	* [XyTools](#xytools)
	* [qgis2leaf](#qgis2leaf)
	* [QSphere](#qsphere)
	* [Layer Combinations](#layer-combinations)
	* [Rectangles Ovals Digitizing](#rectangles-ovals-digitizing)
	* [Affine Transformations](#affine-transformations)
	* [Temporal/Spectral Profile Tool](#temporalspectral-profile-tool)
* [Plugins to review](#plugins-to-review)
	* [Data Plotly for QGIS3](#data-plotly-for-qgis3)
	* [Group Stats](#group-stats)
	* [AutoTrace Plugin (within version 2.14)](#autotrace-plugin-within-version-214)
	* [Descarga de imágenes PNOA ¿?](#descarga-de-imágenes-pnoa)
* [Old plugins](#old-plugins)
	* [Numerical Vertex Edit](#numerical-vertex-edit)
* [QGIS alternative repositories (OLD)](#qgis-alternative-repositories-old)
* [How to manually install a plugin](#how-to-manually-install-a-plugin)
* [More links](#more-links)

<!-- /code_chunk_output -->

## Links

* [QGIS plugins webpage](http://plugins.qgis.org/plugins/)
* [Featured plugins](http://plugins.qgis.org/plugins/featured/)
* [Popular plugins](http://plugins.qgis.org/plugins/popular/)


## Must have Plug-ins

(+) Featured Plugin

### MMQGIS
MMQGIS is a set of plugins for manipulating vector map layers in Quantum GIS: CSV input/output/join, Geocoding, Geometry Conversion, Buffering, Hub Analysis, Simplification Column Modification, Color Ramps, and Simple Animation.

### QuickOSM
QuickOSM allows you to work quickly with OSM datas in QGIS thanks to Overpass API.

### Qgis2threejs
3D visualization powered by WebGL technology and three.js JavaScript library

### qgis2web
Export to an OpenLayers 3/Leaflet webmap (merge of qgis-ol3 and qgis2leaf).

### OpenLayers Plugin
OpenStreetMap, Google Maps, Bing Maps, MapQuest layers and more
[GitHub - sourcepole/qgis-openlayers-plugin: Openlayers plugin for QGIS](https://github.com/sourcepole/qgis-openlayers-plugin)

### QConsolidate
Experimental (May 05-2015)
QConsolidate is a QGIS plugin used to consolidate all project layers into single user defined folder. This can be useful is you decide to share project with many layers from different directories with other person.
[GitHub page](https://github.com/alexbruy/qconsolidate)

### QNEAT3 - QGIS Network Analysis Toolbox 3
Plugin aims to provide sophisticated QGIS Processing-Toolbox algorithms in the field of network analysis.

### Layer Board
View and edit vector and raster properties for the project layers in a table
This plugin helps to see and modify properties for vector and raster layers. It shows the layers in a table sheet, and the user can directly modify some properties such as layer name, title, abstract, minimum and maximum scales. Some actions can be performed on multiple layers at once, such as modifying spatial reference system and scale based visibility. The layers information can be exported to Comma Separated Values (CSV).

### QuickMapServices
Convenient list of basemaps. Easy to add as a layer, easy to create new basemaps and edit. Please leave feedback in issues and contribute new services!


## Interesting Plugins

### QSpatiaLite
Manage your SpatiaLite databases within QGis

### Profile tool
Plots terrain profile

### Table Manager
Manages the attribute table structure (rename, move, clone, delete...) - Similar operations are integrated since v. 2.18

### XyTools
Tools for managing tabular data with x y columns. It can open spreadsheet files (Libre/OpenOffice, Excel) as point layers and save attribute tables as Excel file

You need to install dependecies (Linux):
`sudo apt-get install python-xlwt python-xlrd`

### qgis2leaf
Export your QGIS project to a leaflet based webmap.

### QSphere
Tools for create INSPIRE and ISO 19139 metadata

### Layer Combinations
Store and restore layer visibilities, foldings and zooms in "combinations". The visibilites can then be dynamically assigned to composer maps.

### Rectangles Ovals Digitizing
Helps to create Rectangles,Circles and ovals

### Affine Transformations
Apply affine transformations to selected geometries.

### Temporal/Spectral Profile Tool
Plots profile from raster bands. Based on Profile Tool.
A QGIS plugin for interactive plotting of temporal or spectral information stored in multi-band rasters. Based on Profile tool plugin.


## Plugins to review

### Data Plotly for QGIS3

Check: [Data exploration with Data Plotly for QGIS3 | Free and Open Source GIS Ramblings](https://anitagraser.com/2017/12/06/data-exploration-with-data-plotly-for-qgis3/)

![](https://underdark.files.wordpress.com/2017/12/plotly.png?w=768&h=457)


### Group Stats
Stats and analysis for vector layers data

### AutoTrace Plugin (within version 2.14)
An editing tool for QGIS that allows users to 'trace' new feature geometry based on existing features

### Descarga de imágenes PNOA ¿?
[ver enlace](http://www.euwesttrail.net/PNOA/ortoPNOAhelp.html)


## Old plugins

### Numerical Vertex Edit
Allows to edit a vertex in a numeric way by clicking on it.
Added to latest QGIS versions.


## How to manually install a plugin

Download the plugin directly from the repository url using your browser.  http://pyqgis.org/

Unzip the file. Copy the folder(usually one directory down the extracted folder) to your python plugins directory.

Restart QGIS and now you can see this plugin installed. Just enable this plugin in the Plugin Manager.

## More links

* [Top 15 QGIS Plugins you should use](http://monde-geospatial.com/top-15-qgis-plugins-you-should-use/)
