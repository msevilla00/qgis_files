## QGIS scripts and personal notes

Here I'm uploading some notes, scripts and other files I'm using in my work with QGIS.

Also, you can check some interesting links here:

* [Official QGIS repo in GitHub](https://github.com/qgis)
* [Anita Graser repo in GitHub](https://github.com/anitagraser/)
